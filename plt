#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import sys
from itertools import cycle
import argparse
import re
from scipy.optimize import curve_fit

ls = cycle(["-","--","-.",":"])
mk = cycle(['o','>','<','^','v','s','d'])

# option arguments --------------------------------------------------------------------------------
parser = argparse.ArgumentParser(description='Simple 2D plot')

parser.add_argument('-r','--reverse', action="store_true", default=False,help='reverse plot')
parser.add_argument('-o','--one', action="store_true", dest="one",help='plot on one figure')
parser.add_argument('-P','--point', action="store_true", dest="point",help='plot on one figure')
parser.add_argument('-s','--save', action="store", dest="save",nargs='?',type=str,const='fig.eps',help='save figure')
parser.add_argument('-l','--log', action="store", dest="log",nargs=1,type=int,help='set logscale, 0 for logx, 1 for logy, 2 for logxy')
parser.add_argument('-f','--fit', action="store", dest="fit",nargs=1,type=str,help='fit...')
parser.add_argument('-t','--title', action="store", dest="title",nargs=1,type=str,help='title')
parser.add_argument('-X','--xlabel', action="store", dest="xlabel",nargs=1,type=str,help='xlabel')
parser.add_argument('-Y','--ylabel', action="store", dest="ylabel",nargs=1,type=str,help='ylabel')
parser.add_argument('-x','--xlim', action="store", dest="xl",nargs=2,type=float,help='set xrange')
parser.add_argument('-y','--ylim', action="store", dest="yl",nargs=2,type=float,help='set yrange')
parser.add_argument('-u','--using', action="store", dest="use",nargs=1,type=str,help='choose columns...')
parser.add_argument('-m','--multiple', action="store", dest="multi",nargs='+',type=int,help='plot multiple lines...')
parser.add_argument('-p','--part', action="store", dest="part",nargs=1,type=str,help='choose columns to part data and plot multiple lines...')
parser.add_argument("file", action="store", nargs='*',help='plot filename')
# parser.add_argument("file", action="store", nargs=argparse.REMAINDER,help='plot filename')

args= parser.parse_args()

if args.use:
	use=[int(i) for i in re.split('\W+',args.use[0])]
if args.part:
	part=[int(i) for i in re.split('\W+',args.part[0])]
else:
	part=[]
npa=len(part)
# -------------------------------------------------------------------------------------------------

# function ----------------------------------------------------------------------------------------
def autoscale_y(ax,margin=0.1):
    """This function rescales the y-axis based on the data that is visible given the current xlim of the axis.
    ax -- a matplotlib axes object
    margin -- the fraction of the total height of the y-data to pad the upper and lower ylims"""

    def get_bottom_top(line):
        xd = line.get_xdata()
        yd = line.get_ydata()
        lo,hi = ax.get_xlim()
        y_displayed = yd[((xd>lo) & (xd<hi))]
        h = np.max(y_displayed) - np.min(y_displayed)
        bot = np.min(y_displayed)-margin*h
        top = np.max(y_displayed)+margin*h
        return bot,top

    lines = ax.get_lines()
    bot,top = np.inf, -np.inf

    for line in lines:
        new_bot, new_top = get_bottom_top(line)
        if new_bot < bot: bot = new_bot
        if new_top > top: top = new_top

    ax.set_ylim(bot,top)
# -------------------------------------------------------------------------------------------------

# function ----------------------------------------------------------------------------------------
def pl(x,y,label,reverse):
	if reverse:
		x=1/x
	if args.xl:
		mask=(x>=args.xl[0]) & (x<=args.xl[1])
		x=x[mask]
		y=y[mask]
	if args.fit:
		def func(x,a,b):
			return eval(args.fit[0])
		fit(x,y,func,args.fit[0])
	if args.point:
		plt.plot(x,y,next(mk),label=label)		
	else:
		plt.plot(x,y,ls=next(ls),marker=next(mk),label=label)

def pe(x,y,yerr,label,reverse):
	if reverse:
		x=1/x
	if args.xl:
		mask=(x>=args.xl[0]) & (x<=args.xl[1])
		x=x[mask]
		y=y[mask]
		yerr=yerr[mask]
	if args.fit:
		def func(x,a,b):
			return eval(args.fit[0])
		fit(x,y,func,args.fit[0])
	if args.point:
		plt.errorbar(x,y,yerr=yerr,marker=next(mk),linestyle='None',label=label)
	else:
		plt.errorbar(x,y,yerr=yerr,ls=next(ls),marker=next(mk),label=label)

def fit(x,y,func,label):
	popt, pcov = curve_fit(func, x, y)
	plt.plot(x,y,'ro--')
	x1=np.linspace(x.min(),x.max(),1000)
	plt.plot(x1,func(x1,popt[0],popt[1]),'r-',label=label+",a=%.1E" % popt[0]+",b=%.1E" %popt[1])
	plt.legend(loc='best')
	plt.title("x=1E-10,y=%.1E"%func(1e-10,popt[0],popt[1]))


def fplt(fn):
	ax = plt.subplot(111)
	plt.title(fn.rsplit('.',1)[0])
	feps=fn.rsplit('.',1)[0]+'.eps'

	ar=np.loadtxt(fn)

	if len(ar.shape)==1:
		plt.plot(range(len(ar)),ar,label=fn)
		plt.xlabel('steps')
		plt.legend(loc='best')
		return

	# choose columns to plot --------------------------------------------------
	if args.use:
		if len(use)==2:
			cx=use[0]
			cy=use[1]
			ce=ar.shape[1]
		else:
			cx=use[0]
			cy=use[1]
			ce=use[2]
	else:
		if len(ar.shape)==1:
			cx,cy,ce=0,1,2
		else:
			nrow=ar.shape[0]
			cx=0
			while len(list(set(ar[:,cx])))!=nrow :
				cx+=1
			if args.part:
				cx-=1
			cy=cx+1
			ce=cy+1	
	# -------------------------------------------------------------------------

	# ploting point -----------------------------------------------------------
	if len(ar.shape)==1:
		if ce>=ncol:
			pl(ar[use[cx]],ar[use[cy]],'p',args.reverse)
		else:
			pe(ar[use[cx]],ar[use[cy]],ar[use[ce]],'p',args.reverse)
		return
	# -------------------------------------------------------------------------
	
	nrow=ar.shape[0]
	ncol=ar.shape[1]

	if args.part:
		if len(part)==1:
			c0=part[0]
			col0=list(set(ar[:,c0]))
			col0.sort()
			for el0 in col0:
				ar1=ar[ar[:,c0]==el0]
				ar1=ar1[ar1[:,1].argsort()]
				if ce>=ncol:
					pl(ar1[:,cx],ar1[:,cy],str(el0),args.reverse)
				else:
					pe(ar1[:,cx],ar1[:,cy],ar1[:,ce],str(el0),args.reverse)
	else:
		if ce>=ncol:
			pl(ar[:,cx],ar[:,cy],fn,args.reverse)
		else:
			pe(ar[:,cx],ar[:,cy],ar[:,ce],fn,args.reverse)

	plt.legend(loc='best')
	if args.log:
		if args.log[0]==0:
			ax.set_xscale('log')
		elif args.log[0]==1:
			ax.set_yscale('log')
		else:
			ax.set_xscale('log')
			ax.set_yscale('log')
	# if args.xl:
		# margin=(args.xl[1]-args.xl[0])*0.1
		# plt.xlim(args.xl[0]-margin,args.xl[1]+margin)
		# autoscale_y(ax)
	if args.yl:
		plt.ylim(args.yl[0],args.yl[1])

# -------------------------------------------------------------------------------------------------

for i in range(len(args.file)):
	if not args.one:
		plt.figure()
	fplt(args.file[i])

if args.title:
	plt.title(args.title[0])
if args.xlabel:
	plt.xlabel(args.xlabel[0])
if args.ylabel:
	plt.ylabel(args.ylabel[0])

if args.save:
	plt.savefig(args.save)

plt.show()
